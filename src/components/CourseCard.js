// import { useState, useEffect } from 'react';
import { Link } from 'react-router-dom';
import {Row, Col, Card, Button} from 'react-bootstrap';


// destructuring - is a way of isolating properties
// const myObj = {name: "Jino", occupation: "Instructor"}
// let {occupation} = myObj
// console.log(occupation)	ans= "Instructor"
// Puwedeng i reassign ang value ng destructured variable


/*
	Using Props

	puwede nating gamiting ang prop ng ganito,
*/

//export default function CourseCard(props) {
//	let { name, description, price } = props.courseProp;

	//- ang (props) ay ang value "argument".
	//- ang props.courseProp ay galing sa
	//	Course.js > return <CourseCard courseProp={course}/>

	//	ito yung inasign nating name ng magiging props natin.
	//	Yung {course} ay galing sa course.js na nasa loob ng data folder.


/*
	pero mas maganda at organize ang code natin sa ganito.
*/
export default function CourseCard({courseProp}) {
	//ang courseProp ay galing sa Course.js > return <CourseCard courseProp={course}/>
	/*console.log(props)*/
	let { name, description, price, _id } = courseProp;



//syntax:
//const [state, setState] = useState(default state)

//state(ang klase ng variable na ito: const [state, setState] = useState(default state)): a special kind of variable (can be name anything) that React uses to render/re-render components when neede

//state setter: State setter are the ONLY way to change a state's value. By convention, they are named after the state.

//default state: The state's initial value on component mount. befoer a component mounts, a state actually defaults to undefined, then is changed to its default state.

//array destructuring to get the state and the setter
	// const [count, setCount] = useState(0);
	// const [seats, seatsCount] = useState(10);
	// function enroll() {
	// 	//my answer
	// 	/*setCount(count + 1);
	// 	seatsCount(seats - 1)
	// 	console.log('Enrolless' + count)
	// 	if(seats === 0) {
	// 		alert(`No more seats available`)
	// 	}*/
	////Sir Jino Answer
	// 	if(seats === 0) {
	// 		alert(`No more seats available`)
	// 	} else {
	// 		setCount(count + 1);
	// 		seatsCount(seats - 1);
	// 	}
	// }
	// 	if(count !== 10) {
	// 		setCount(count + 1);
	// 		seatsCount(seats - 1);
	// 	}
	// }

	// function enroll() {
	// 	if(count !== 10) {
	// 		setCount(count + 1);
	// 		seatsCount(seats - 1);
	// 	}
	// }



/*
	Apply useEffect hook
		useEffect makes any given code block happen when a state changes AND when a component first mounts (such as on initial page load)

	Syntax:
		useEffect(function, [dependencies])
*/
	
	// useEffect(()=> {
	// 	if(seats === 0) {
	// 		alert(`No more seats available`)
	// 	}
	// },[count, seats])

/*

	Activity:
	- Create a state hook that will represent the number of available seats for ech course.
	- it should default to 10, and decrement by 1 each time a student enrolls.
	- Add a condition that will show an alert that no more seats are available if the seats state is 0.

*/

	return (
		<Row className="mt-3 mb-3">

			<Col>
				<Card className="cardHighlight p-3">
      				{/*<Card.Img variant="top" src="holder.js/100px180" />*/}
      				<Card.Body>
        				<Card.Title>{name}</Card.Title>
       				 	<Card.Subtitle>Description:</Card.Subtitle>
       				 	<Card.Text>{description}</Card.Text>
       				 	<Card.Subtitle>Price:</Card.Subtitle>
       				 	<Card.Text>PHP {price}</Card.Text>
        				<Button as={Link} to={`/courses/${_id}`}>Enroll</Button>
      				</Card.Body>
    			</Card>
			</Col>
		</Row>
	)
};