import { useState, useContext } from 'react';
import { Navbar, Nav, Container } from 'react-bootstrap'
import { Link } from 'react-router-dom';
import UserContext from '../UserContext';

export default function AppNavBar(){

	// const [user, setUser] = useState(localStorage.getItem("email"));
	// console.log(user);

	const { user } = useContext(UserContext)

	return(
		<Navbar bg="light" expand="lg">
		  <Container>
		    <Navbar.Brand as={Link} to="/">B211 Course Booking App</Navbar.Brand>
		    <Navbar.Toggle aria-controls="basic-navbar-nav"/>
		    <Navbar.Collapse id="basic-navbar-nav">
			
		      <Nav className="ms-auto">
		        <Nav.Link as={Link} to="/">Home</Nav.Link>
		        <Nav.Link as={Link} to="/courses">Courses</Nav.Link>
		        
		        {(user.id !== null) ?
		        	<Nav.Link as={Link} to="/logout">Logout</Nav.Link>
		        	:
		        	<>
		        		<Nav.Link as={Link} to="/login">Login</Nav.Link>
		        		<Nav.Link as={Link} to="/register">Register</Nav.Link>
		    		</>
		    	}
		      </Nav>
		    </Navbar.Collapse>
		  </Container>
		</Navbar>
	)
}
