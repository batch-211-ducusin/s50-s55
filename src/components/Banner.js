import {Row, Col, Button} from 'react-bootstrap';
import { Link } from 'react-router-dom';

/*export default function Banner({bannerProp}) {
	return (
		<Row>
			<Col>
				<h1>Zuitt Coding Bootcamp</h1>
				<p>Opportunities for everyone, evreywhere!</p>
				<Button variant="primary">Enroll now!</Button>
			</Col>
		</Row>
	)
}*/

export default function Banner({bannerProp}) {

	// Destructured model of bannerProp
	const { title, content, destination, label } = bannerProp;

	return (
		// Text-center to place all text and component in the center/middle.
		// my = mt and mb = margin top and margin bottom
		<div className="text-center my-5">
			<Row>
				<Col>
					<h1>{title}</h1>
					<p>{content}</p>
					<Button as={Link} to={destination} variant="primary">{label}</Button>
				</Col>
			</Row>
		</div>
	)
}