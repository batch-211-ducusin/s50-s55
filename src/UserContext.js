import React from 'react';
// Creation of Context Object
// Ito ay default codes for context using.
const UserContext = React.createContext();

// The "Provider" component allows other components to consume/use the context object and supply the necessary information needed.
// Si provider ang dahilan kung bakit nakakapag store at nagagamit natin ang ating data sa ibang page.
export const UserProvider = UserContext.Provider;


export default UserContext;