// import coursesData from '../data/coursesData';
import { useEffect, useState } from 'react';
import CourseCard from '../components/CourseCard';

export default function Courses() {
	
	/*console.log(coursesData)*/

	// using the map array method, we can loop through our courseData and dynamically render any number of CourseCards depending on how many array elements are present in our data.
	// map returns an array, which we can display in hte page via the component function's return statement.
	//forEach() returns each elements
	//map() returns array
	
	// Props are a way to pass any valid JS data from parent component to child component.
	
	// You can pass as many props as you want. Even functions can be passed as props.
	/*function myFunc() {
		return 1 + 1
	}*/


	const [courses, setCourses] = useState([])

	useEffect(()=> {
		fetch('http://localhost:4000/courses/all')
		.then(res=>res.json())
		.then(data=>{

			const coursesArr = (data.map(course=> {
				return (
					<CourseCard courseProp={course} key={course._id}/>
				)
			}))
			setCourses(courseArr)
		})	
	}, [course])
	
	
	return (
		<>
			{/*<h1>Courses</h1>*/}
			{/*<CourseCard/>*/}
			{coursesArr}{/*yung number ng course na nadisplay ay kinuha nya sa data, kung ilan yung object sa loob ng data, yun din ang number na ididisplay nya.*/}
		</>
	)
}