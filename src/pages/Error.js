import { Button } from 'react-bootstrap';

//My Code
/*export default function Error() {

	return(
		<>
			<div className="container mt-5">
				<div className="row justify-content-center text-center">
					<div className="col-6">
						<h1>404 - Not Found</h1>
						<p>The page that you are looking for can't be found.</p>
						<Button href="/">Back Home</Button>
					</div>
				</div>
			</div>
		</>
	)
};*/

//Sir Rome Code
import Banner from '../components/Banner.js';

export default function Error() {

	const data = {
		title: "404 - Not Found",
		content: "The page you are looking for can't be found.",
		destination: "/",
		label: "Back Home"
	}

	return (
		// props name is up to the developer, can be anything.
		<Banner bannerProp={data}/>
	)
}