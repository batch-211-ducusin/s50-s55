import { Form, Button } from 'react-bootstrap';
// Complete (3) Hooks of React
import { useState, useEffect, useContext } from 'react';
import UserContext from '../UserContext';
import { Navigate } from 'react-router-dom';
import Swal from 'sweetalert2';

export default function Login() {
	const [email, setEmail] = useState('');
	const [password, setPassword] = useState('');
	const [isActive, setIsActive] = useState('');

	console.log(email);
	console.log(password);



	// Allows us to consume the User Context/Data and its properties for validation.

	const { user, setUser } = useContext(UserContext);

	function authenticate(e) {
		e.preventDefault();

		// gagawa tayo ng request para maintegrate natin si frontend at backend using fetch
		// Si fetch ay may dalawang argument. Una ay yung link pangalawa ay yung options like method, headers, etc
		fetch('http://localhost:4000/users/login', {
			method: 'POST',
			headers: {
				'Content-Type': 'application/json'
			},
			body: JSON.stringify({
				email: email,
				password: password
			})
		})
		.then(res=>res.json())
		.then(data=>{
			console.log(data);
			if(typeof data.access !== "undefined"){
				//kapag hindi daw undefined ang token i store ang token sa localstrogae.
				localStorage.setItem('token', data.access)
				retrieveUserDetails(data.access)

				Swal.fire({
					title: "Login Successful",
					icon: "success",
					text: "Welcome to Booking App of 211!"
				})

			} else {
				
				Swal.fire({
					title: "Authentication Failed!",
					icon: "error",
					text: "Check your credentials!"
				})
			}
		})
		//conditional statement para mastore ang token sa localstorage

		const retrieveUserDetails = (token)=> {
			//ang irereceive nya ay ang token once na mag login si user.
			fetch('http://localhost:4000/users/details', {
				headers: {
					Authorization: `Bearer ${token}`
				}
			})
			.then(res=>res.json())
			.then(data=>{
				console.log(data);

				setUser({
					id: data._id,
					isAdmin: data.isAdmin
				})
			})
		}





		// localStorage.setItem("email", email);
		//This allow us to store email
		// si setItem, set the email of the authenticated user in the local storage
		/*
			Syntax:
				localStorage.setItem("propertyName", value)
		*/

		// Set the global user state to have properties obtained from local storage.
		// This will pass the datat to the UserContext which is ready to use from all different endpoints/pages.
		// setUser({
		// 	email: localStorage.getItem('email')
		// })

		setEmail("");
		setPassword("");

		// alert(`${email} You are now logged in.`)
	}

	useEffect(()=> {
		if(email !== "" && password !== "") {
			setIsActive(true);
		} else {
			setIsActive(false);
		}
	},[email, password])

	return(

		// Conditional Rendering
		// LOGIC - if there is a user logged-in in the web application, endpoint "/login" should not be accessible. The user should be navigated to courses tab instead.
		(user.id !== null)
		?
		<Navigate to="/courses" />
		// ELSE - if the localStorage is empty, th user is allowed  to access the login page.
		:
		<div className="container mt-5">
			<div className="row justify-content-center">
				<div className="col-6">
					<h1>Login</h1>
					<Form onSubmit={(e)=> authenticate(e)}>

						<Form.Group className="mb-3" controlId="userEmail">
    						<Form.Label>Email address</Form.Label>
        					<Form.Control
        						type="email"
        						placeholder="Enter email"
        						value={email}
        						onChange={e=> setEmail(e.target.value)}
        						required
       						/>
    					</Form.Group>

    					<Form.Group className="mb-3" controlId="password1">
        					<Form.Label>Password</Form.Label>
        					<Form.Control
        						type="password"
        						placeholder="Password"
        						value={password}
        						onChange={e=> setPassword(e.target.value)}
        						required
       						/>
      					</Form.Group>

      					{ isActive ? 
    					<Button
    						variant="success"
    						type="submit"
    						id="submitBtn"
    					>Login</Button>
    					:
    					<Button
    						variant="danger"
    						type="submit"
    						id="submitBtn"
    						disabled
    					>Login</Button>
    					}

					</Form>
				</div>
			</div>
		</div>
	)
};