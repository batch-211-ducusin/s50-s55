// import Button from 'react-bootstrap/Button';
// import Form from 'react-bootstrap/Form';

import { Form, Button } from 'react-bootstrap';
import { useState, useEffect, useContext } from 'react';
import { Navigate } from 'react-router-dom';
import UserContext from '../UserContext';


//Define state hooks for all input fields and an "isActive" state for conditional rendering of the submit btn

export default function Register() {
	// Create state hooks to store the values of the input fields
	const [firstName, setFirstName] = useState('');
	const [lastName, setLastName] = useState('');
	const [email, setEmail] = useState('');
	const [mobileNo, setMobileNo] = useState('');
	const [password1, setPassword1] = useState('');
	const [password2, setPassword2] = useState('');

	//Create a state to determine whether the submit button is enabled or not.
	const [isActive, setIsActive] = useState('');

	console.log(firstName);
	console.log(lastName);
	console.log(email);
	console.log(mobileNo);
	console.log(password1);
	console.log(password2);

// Itong function na ito ang mag e-empty ulit ng field kapag pinindot na ang submit. Sa lahat ng form, nilagyan natin ng "onChange={e=> setFirstName(e.target.value)}" sa Form.Control
	function registerUser(e) {

		e.preventDefault();

		setFirstName("");
		setLastName("");
		setEmail("");
		setMobileNo("");
		setPassword1("");
		setPassword2("");

		alert("Thank you for registering!")
	};

	/*
		Two Way Binding
		- it is done so that we can assure that we can save the input into our states as we type into the input.

		- This is done so that we don't have to save it just before we submit.

		e.target
		- current element where the event happens.

		e.target.value
		- current value of the element where the event happened.
	*/

	const { user, setUser } = useContext(UserContext);

// Itong useEffect na to ang mag-sasabi kung na fill na ang mga field at gagawin nyang active ang button. Kailangan natin mag define ng state variable para sa isActive na button, parang ganito: const [isActive, setIsActive] = useState(''); . Nilagay natin sa ternary operator ang dalawang button natin (isActive ? <diabledBtn> : <abledBTN>) para ma-apply natin si useEffect().
	useEffect(()=> {
		if((firstName !== "" && lastName !== "" && mobileNo.length === 11 && email !== "" && password1 !== "" && password2!== "") && (password1 === password2)) {
			setIsActive(true);
		} else {
			setIsActive(false);
		}
	},[firstName, lastName, email, mobileNo, password1, password2])

  return (
  	(user.id !== null)
		?
		<Navigate to="/courses" />
		:
    <Form onSubmit={(e)=> registerUser(e)}>


    <Form.Group className="mb-3" controlId="firstName">
        <Form.Label>First Name</Form.Label>
        <Form.Control
        	type="text"
        	placeholder="First Name"
        	value={firstName}
        	onChange={e=> setFirstName(e.target.value)}
        	required
        />
    </Form.Group>

    <Form.Group className="mb-3" controlId="lastName">
        <Form.Label>Last Name</Form.Label>
        <Form.Control
        	type="text"
        	placeholder="Last Name"
        	value={lastName}
        	onChange={e=> setLastName(e.target.value)}
        	required
        />
    </Form.Group>

    <Form.Group className="mb-3" controlId="mobileNo">
        <Form.Label>Mobile Number</Form.Label>
        <Form.Control
        	type="number"
        	placeholder="+63XXXXXXXXXX"
        	value={mobileNo}
        	onChange={e=> setMobileNo(e.target.value)}
        	required
        />
    </Form.Group>


    <Form.Group className="mb-3" controlId="userEmail">
    	<Form.Label>Email address</Form.Label>
        <Form.Control
        	type="email"
        	placeholder="Enter email"
        	value={email}
        	onChange={e=> setEmail(e.target.value)}
        	required
       	/>
        <Form.Text className="text-muted">
          We'll never share your email with anyone else.
        </Form.Text>
    </Form.Group>


      <Form.Group className="mb-3" controlId="password1">
        <Form.Label>Password</Form.Label>
        <Form.Control
        	type="password"
        	placeholder="Password"
        	value={password1}
        	onChange={e=> setPassword1(e.target.value)}
        	required
        />
      </Form.Group>


      <Form.Group className="mb-3" controlId="password2">
        <Form.Label>Password</Form.Label>
        <Form.Control
        	type="password"
        	placeholder="verify password"
        	value={password2}
        	onChange={e=> setPassword2(e.target.value)}
        	required
        />
      </Form.Group>

    	{ isActive ? 
    	<Button
    		variant="success"
    		type="submit"
    		id="submitBtn"
    		to="/login"
    	>Submit</Button>
    	:
    	<Button
    		variant="danger"
    		type="submit"
    		id="submitBtn"
    		disabled
    	>Submit</Button>
    	}

    </Form>
  );
}

/*
	mini activity:
		- add the additional fields
			first name
			last name
			mobileNo
*/

/*
	Activity
	Simulate a user login and authenticated
*/